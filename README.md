# GitLab Ultimate or Gold for Open Source Projects

We take our responsibility of open source stewardship very seriously
(https://about.gitlab.com/2016/01/11/being-a-good-open-source-steward/ and https://about.gitlab.com/stewardship/).

GitLab exists today in large part thanks to the work of hundreds of thousands of open source contributors
around the world. To give back to this community who gives us so much, we want to help teams be
more efficient, secure, and productive. We believe the best way for them to achieve this is by
using as many of the capabilities of GitLab as possible.

It has already been the case for years that that any public project on GitLab.com gets all Gold features. We
are happy to now offer a complimentary license to GitLab Ultimate (self-hosted) or subscription to GitLab
Gold (SaaS) to all open source projects.

## Here's how to apply

1.   Create a gitlab.com account for your open source project: https://gitlab.com/users/sign_in
1.   Edit this file and add an entry to the [Open source projects using GitLab Ultimate or Gold](#open-source-projects-using-gitlab-ultimate-or-gold) 
section at the bottom of this page (all lines required):

     ```
     ### Project name
     A short description of what you do and why.
     https://myawesomeproject.org
     Ultimate or Gold?
     ```
     
1.   Commit your changes to a new branch and start a new Merge Request.

## Requirements

To apply:
- You need to be a project lead or a core contributor for an active open source project.
- Your project needs to use a standard open source license and must be non-commercial.
- Your project should not have paid support or pay contributors.

If you or your company work on commercial projects, consider our [plans for businesses](https://about.gitlab.com/pricing/). 
If you're not sure if your project meets these requirements, please [contact our support team](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=334447) for help.

We'll review all requests and accept them at our discretion. If accepted, your project will stay listed below.

## License/subscription details

- You'll receive a 1 year license for GitLab Ultimate or subscription for GitLab Gold.
- Support is not included, but can be purchased for 95% off, at $4.95/user/month. [Contact Sales](https://ultimate-free-post.about.gitlab.com/sales/) for that.
- Your license/subscription can be renewed each year if your project still meets the requirements.
   - [Contact Sales](https://ultimate-free-post.about.gitlab.com/sales/) 30 days before your license/subscription ends.
- Licenses and subscriptions cannot be transferred or sold.

## Open source projects using GitLab Ultimate or Gold
```
### Alchemy Viewer
A client for SecondLife/OpenMetaverse protocol compatible virtual world platforms.
https://www.alchemyviewer.org
Ultimate
```